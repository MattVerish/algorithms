import numpy as np
def mergeSort(array):
	n = len(array)
	if n > 1:
		return merge(mergeSort(array[:n//2]), mergeSort(array[n//2:]))
	return array

def merge(array1, array2):
	l, k = len(array1), len(array2)
	if l == 0:
		return array2
	elif k == 0:
		return array1
	if array1[0] <= array2[0]:
		return [array1[0]] + merge(array1[1:], array2)
	else:
		return [array2[0]] + merge(array1, array2[1:])
