import numpy as np
from MergeSort import mergeSort


print("Testing mergeSort.")
for i in range(100):
	correct = 100
	array = np.random.randint(1000000, size=np.random.randint(100)).tolist()
	mergeSorted = mergeSort(array)
	defaultSorted = np.sort(array)
	if (not np.array_equal(mergeSorted, defaultSorted)):
		print("Failed test: %d mergeSorted array not equal to numpy sorted." %i + 1)
		correct -= 1
print("Passed %d/100 cases." %correct)

